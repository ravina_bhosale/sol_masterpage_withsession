﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_MasterPage.Models
{
    public class CustomerEntity
    {
        public decimal? UserId { get; set; }
        
        public String FirstName { get; set; }
        
        public String LastName { get; set; }
        
        public String ImagePath { get; set; } 

        public CustomerLoginEntity Login { get; set; }
    }
}