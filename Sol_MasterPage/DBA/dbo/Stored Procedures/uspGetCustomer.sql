﻿CREATE PROCEDURE [dbo].[uspGetCustomer]
	
	@Command Varchar(50)=NULL,

	@UserId Numeric(18,0)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

	AS

	BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='GetCustomerData'

		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY

						SELECT 
							C.FirstName,
							C.LastName,
							C.ImagePath
								FROM tblCustomer AS C
									where c.UserId=@UserId
					SET @Status=1
					SET @Message='Select all Data'

				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

			SET @ErrorMessage=ERROR_MESSAGE()
			SET @Status=0
			SET @Message='Stored Proc execution Fail'

			RAISERROR(@ErrorMessage,16,1)
					
			ROLLBACK TRANSACTION

			END CATCH
	END

	END

