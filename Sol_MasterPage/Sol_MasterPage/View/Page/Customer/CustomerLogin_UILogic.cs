﻿using Newtonsoft.Json;
using Sol_MasterPage.DAL;
using Sol_MasterPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_MasterPage.View.Page.Customer
{
    public partial class LoginPage
    {
        //#region declaration
        //private CustomerDal customerDalObj = null;
        //#endregion

        #region public Method
        public async Task<CustomerEntity> MapCustomerData()
        {
            return await Task.Run(async () =>
            {
                try
                {

                    // Create an instance of Uuser Entity and Map control data into Poco
                    CustomerEntity customerEntityObj = new CustomerEntity()
                    {
                        Login = new CustomerLoginEntity()
                        {
                            UserName = txtUserName.Text,
                            Password = txtPassword.Text
                        }
                    };

                    var result = await new CustomerDal().CustomerLoginData(customerEntityObj);
                    if (result is CustomerEntity)
                    {
                        //create a session 
                        string userJsonData = JsonConvert.SerializeObject(customerEntityObj);
                        Session["CustomerLoginInfo"] = userJsonData;

                        Server.Transfer("~/View/Page/CustomerProfile/CustomerProfilePage.aspx", false);
                    }
                    else // false Part .. Login Failed
                    {
                        // show Error Message
                        lblMessage.Text = result;
                    }
                    return result;
                }
                catch (Exception)
                {
                    throw;
                }
            });
           
        }

        //public async Task BindandRedirection(dynamic result)
        //{
        //    await Task.Run(async() =>
        //    {
        //        try
        //        {


        //            // true part..login Success
        //            if (result is CustomerEntity)
        //            {
        //                // Create a Session
        //               await this.CreateUserSession(result as CustomerEntity);

        //                Server.Transfer("~/View/Page/CustomerProfile/CustomerProfilePage.aspx", false);
        //            }
        //            else // false Part .. Login Failed
        //            {
        //                // show Error Message
        //                lblMessage.Text = result;
        //            }


        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    });
               
        //}

        //public async Task CreateUserSession(CustomerEntity customerEntityObj)
        //{
        //    await Task.Run(() =>
        //    {
        //        try
        //        {


        //            // Serialize User Entity Object into JSON
        //            string userJsonData = JsonConvert.SerializeObject(customerEntityObj);

        //            // Create a Session Value
        //            Session["CustomerLoginInfo"] = userJsonData;

        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    });
          
        //}

        //public async Task BindData()
        //{
        //    //var customerDalObj = new CustomerDal();

        //    var customerDalObj = new CustomerDal()?.CustomerLoginData(await this.MapCustomerData());

        //   await this.BindandRedirection(customerDalObj);
        //}
        #endregion
    }
}