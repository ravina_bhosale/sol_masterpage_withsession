﻿using Newtonsoft.Json;
using Sol_MasterPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_MasterPage
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["CustomerLoginInfo"] != null)
            {
                // get Session Value
                string customerJsonData = Session["CustomerLoginInfo"] as string;

                // Deserialize Json String into User Entity Object
                CustomerEntity customerEntityObj = JsonConvert.DeserializeObject<CustomerEntity>(customerJsonData);

                // Bind Full Name into Label Control

                lblFullName.Text = new StringBuilder()
                                                    .Append(customerEntityObj.FirstName)
                                                    .Append(" ")
                                                    .Append(customerEntityObj.LastName)
                                                    .ToString();
                imgCustomerProfilePic.ImageUrl = customerEntityObj.ImagePath;
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Remove("CustomerLoginInfo");
                Session.Abandon();

                Server.Transfer("~/View/Page/Customer/LoginPage.aspx", false);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}