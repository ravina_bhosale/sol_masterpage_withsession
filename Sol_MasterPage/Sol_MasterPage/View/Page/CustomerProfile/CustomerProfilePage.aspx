﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="CustomerProfilePage.aspx.cs" Inherits="Sol_MasterPage.View.Page.CustomerProfile.CustomerProfilePage" %>

<%@ MasterType VirtualPath="~/Main.Master" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updatePanel" runat="server">

        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:TextBox ID="txtFirstName" runat="server" placeHolder="FirstName"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtLastName" runat="server" placeHolder="LastName"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="btnCancel_Click"></asp:Button>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
