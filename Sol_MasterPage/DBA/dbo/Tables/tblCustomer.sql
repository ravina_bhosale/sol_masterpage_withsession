﻿CREATE TABLE [dbo].[tblCustomer] (
    [UserId]    NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50)   NULL,
    [LastName]  VARCHAR (50)   NULL,
    [ImagePath] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_tblCustomer] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

