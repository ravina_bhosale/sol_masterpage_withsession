﻿CREATE PROCEDURE [dbo].[uspLogin]
	
	@Command Varchar(50)=NULL,

	@UserId Numeric(18,0)=NULL,

	@UserName Varchar(50)=NULL,
	@Password Varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

	AS

	BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='Login'

		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY


				IF  exists(select CL.UserName,CL.Password
							 from tblCustomerLogin as CL 
								where CL.UserName=@UserName and CL.Password=@Password)
						BEGIN
							SET @Status=1
							SET @Message='Login Success'

						SELECT 
							C.FirstName,
							C.LastName,
							C.ImagePath
						FROM tblCustomerLogin AS CL
							INNER JOIN
								tblCustomer AS C
									ON
										CL.UserID=C.UserID
											WHERE CL.UserName=@UserName and CL.Password=@Password
						END
				ELSE	
						BEGIN
							SET @Status=0
							SET @Message='User name and Password are wrong'
						END

				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

			SET @ErrorMessage=ERROR_MESSAGE()
			SET @Status=0
			SET @Message='Stored Proc execution Fail'

			RAISERROR(@ErrorMessage,16,1)
					
			ROLLBACK TRANSACTION

			END CATCH
	END

	END

