﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_MasterPage.Models
{
    public class CustomerLoginEntity
    {
        public decimal? UserId { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }
    }
}