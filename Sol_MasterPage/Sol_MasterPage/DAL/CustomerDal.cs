﻿using Sol_MasterPage.DAL.ORD;
using Sol_MasterPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_MasterPage.DAL
{
    public class CustomerDal
    {
        #region Declaration
        private CustomerDcDataContext dc = null;
        #endregion

        #region Constructor
        public CustomerDal()
        {
            dc = new CustomerDcDataContext();
        }
        #endregion

        #region Public Method
        public async Task<dynamic> CustomerLoginData(CustomerEntity customerEntityObj)
        {
            int? status = null;
            String message = null;

            return await Task.Run(() =>
            {
                return
                dc
                ?.uspLogin
                (
                    "Login",
                    customerEntityObj?.UserId,
                    customerEntityObj?.Login?.UserName,
                    customerEntityObj?.Login?.Password,
                    ref status,
                    ref message
                    )
                    ?.AsEnumerable()
                    ?.Select((leGetCustomerLogin) => new CustomerEntity()
                    {
                        UserId = leGetCustomerLogin?.UserId,
                        FirstName = leGetCustomerLogin?.FirstName,
                        LastName = leGetCustomerLogin?.LastName,
                        ImagePath = leGetCustomerLogin?.ImagePath
                    }).FirstOrDefault();
            });
        }

        public async Task<CustomerEntity> GetCustomerData(CustomerEntity customerEntityObj)
        {
            int? status = null;
            String message = null;

            return await Task.Run(() =>
            {
                return
                dc
                ?.uspGetCustomer
                (
                    "GetCustomerData",
                    customerEntityObj?.UserId,
                    ref status,
                    ref message
                    )
                    ?.AsEnumerable()
                    ?.Select((leGetCustomerData) => new CustomerEntity()
                    {
                        UserId = leGetCustomerData?.UserId,
                        FirstName = leGetCustomerData?.FirstName,
                        LastName = leGetCustomerData?.LastName,
                        ImagePath = leGetCustomerData?.ImagePath
                    })
                    ?.ToList()
                    ?.FirstOrDefault();
            });
               
        }
        #endregion
    }
}