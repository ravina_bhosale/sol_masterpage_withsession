﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="Sol_MasterPage.View.Page.Customer.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

       table
        {
            width : 50%;
            margin:auto;
            border-collapse:collapse;
        }
       .textBox
       {
            border:2px;
            border-color:black;
            border-style:solid;
            border-radius:5px;
            width:50%;
            height:30px;
            padding:2px;

       }

        .button:hover {

            background-color: #008CBA;
            color: white;

        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="scriptManager" runat="server">
            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server" placeHolder="UserName" CssClass="textBox"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtPassword" runat="server" placeHolder="Password" CssClass="textBox"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Login" CssClass="button" Height="30px" Width="90px" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                 <Triggers>
                    <asp:PostBackTrigger ControlID="btnSubmit" />
                </Triggers>

            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
